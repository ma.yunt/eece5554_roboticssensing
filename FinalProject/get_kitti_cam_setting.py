import argparse
import pathlib

def main(args):
    dataset_dir = pathlib.Path(args.dataset)
    cali_pth = dataset_dir/'calib_cam_to_cam.txt'
    cam_name = args.cam

    if not dataset_dir.is_dir():
        raise FileNotFoundError('Dataset directory not valid.')
    if not cali_pth.exists():
        raise FileExistsError('Calibration FIle not found.')
    
    with open(cali_pth, 'r') as f:
        lines = f.readlines()
        for line in lines:
            if 'P_rect_'+cam_name in line:
                cam_matrix = line.strip()[11:].split(' ')
                cam_matrix = [float(x) for x in cam_matrix]
                fx = cam_matrix[0]
                cx = cam_matrix[2]
                fy = cam_matrix[5]
                cy = cam_matrix[6]
    print('#------------------\n#ORB_SLAM3 cam setting\n#------------------')
    print('Camera.type: "PinHole"')  
    print('Camera.fx: {}'.format(fx))
    print('Camera.fy: {}'.format(fy))
    print('Camera.cx: {}'.format(cx))
    print('Camera.cy: {}'.format(cy))
    print('Camera.k1: 0.0\nCamera.k2: 0.0\nCamera.p1: 0.0\nCamera.p2: 0.0')
    print('Camera.fps: 10.0\nCamera.RGB: 1\nCamera.width: 1241\nCamera.height: 37')


if __name__ ==  '__main__':
    parser = argparse.ArgumentParser(add_help='get camera setting for ORB_SLAM3 from kitti calibration file')
    parser.add_argument('-d', '--dataset', help='dataset dir',
                        required=True)
    parser.add_argument('-c', '--cam', help='name of cam 00=lgray, 01=rgray, 02=lrgb, 03=rrgb',
                        required=True, choices=['00', '01', '02', '03'])
    args = parser.parse_args()
    main(args)
