%% Read analysis csv file provided by Tommy Ma
data = readmatrix('datasets/kitti_2011_09_30_0018_analysis.csv');

% Define column indices for specific types of data
index_time_stamp = 1;
index_lat = 2; 
index_lon = 3; 
index_alt = 4;
index_slam_x = 5;
index_slam_y = 6;
index_slam_z = 7;

%% Convert latitude and longitude to UTM using function from
% https://www.mathworks.com/matlabcentral/fileexchange/45699-ll2utm-and-utm
% 2ll?s_tid=srchtitle
lat = data(:, index_lat);
lon = data(:, index_lon);

if (length(lat) == length(lon))
    utm_x = zeros(length(lon), 1);
    utm_y = zeros(length(lat), 1);
    start_index = 1;
    found_start = false;
    
    % find the first value in the lat and lon arrays that is not NaN
    while (~found_start)
        if (isnan(lat(start_index)) || isnan(lon(start_index)))
            start_index = start_index + 1;
        else
            found_start = true;
            [x1, y1, zone] = ll2utm(lat(start_index), lon(start_index));
        end
    end
    
    for i = start_index:length(lat)
        [x, y, zone] = ll2utm(lat(i), lon(i));
        utm_x(i) = x - x1;
        utm_y(i) = y - y1;
    end
else
    error('Error: Latitude and Longitude lengths not equal');
end

% remove NaN values
utm_x_no_nan = utm_x(~isnan(utm_x));
utm_y_no_nan = utm_y(~isnan(utm_y));

%% Obtain x, y, z from SLAM

slam_x = data(:, index_slam_x);
slam_y = data(:, index_slam_y);
slam_z = data(:, index_slam_z);

% remove NaN values
slam_x_no_nan = slam_x(~isnan(slam_x));
slam_y_no_nan = slam_y(~isnan(slam_y));
slam_z_no_nan = slam_z(~isnan(slam_z));

% calculate suitable scale value in X
gps = mean(utm_x_no_nan(1573:1828));
slam = mean(slam_x_no_nan(602:724));
scale = gps / slam;
slam_x = scale .* slam_x;

% calculate suitable scale value in Y
gps = mean(utm_y_no_nan(1573:1828));
slam = mean(slam_y_no_nan(602:724));
scale = gps / slam;
slam_y = scale .* slam_y;

% rotation
d = 3.5;
R = [cosd(d), -sind(d); sind(d), cosd(d)];
for i=1:length(slam_x)
    rotVal = R * [slam_x(i); slam_y(i)];
    slam_x(i) = rotVal(1);
    slam_y(i) = rotVal(2);
end

% extra scale
slam_x = 0.9 .* slam_x;
slam_y = 0.35 .* slam_y;

%% Error analysis
absolute_error_x = [];
absolute_error_y = [];

for i=1:length(slam_x)
    if ~isnan(slam_x(i))
        error = utm_x(i+1) - slam_x(i);
        absolute_error_x = [absolute_error_x; error];
    end
end

for i=1:length(slam_y)
    if ~isnan(slam_y(i))
         error = utm_y(i+1) - slam_y(i);
        absolute_error_y = [absolute_error_y; error];
    end
end

%% Plot Lat and Lon, UTM Northing and Easting, SLAM x and y
figure(1);

subplot(2, 2, 1);
scatter(lon, lat, 5, 'filled', 'r');
title('Latitude vs. Longitude');
xlabel('Longitude (degrees)');
ylabel('Latitude (degrees)');

subplot(2, 2, 2);
scatter(utm_x, utm_y, 5, 'filled', 'b');
title('UTM Northing vs. Easting');
xlabel('UTM Easting (meters)');
ylabel('UTM Northing (meters)');

subplot(2, 2, 3);
scatter(slam_x_no_nan, slam_y_no_nan, 5, 'filled', 'm');
title('SLAM Estimation');
xlabel('X');
ylabel('Y');

subplot(2, 2, 4);
hold on;
scatter(utm_x, utm_y, 5, 'filled', 'b');
scatter(slam_x, slam_y, 5, 'filled', 'm');
title('GPS vs. SLAM');
xlabel('X (meters)');
ylabel('Y (meters)');
legend('GPS', 'SLAM');

%% Plot error
figure(2);

subplot(2, 1, 1);
histogram(absolute_error_y, 10, 'FaceColor', 'r');
title('Absolute Error in Y (Latitude)');

subplot(2, 1, 2);
histogram(absolute_error_x, 10, 'FaceColor', 'r');
title('Absolute Error in X (Longitude)');

%% Average translation error
fprintf('Average translation error in Y: %f\n', mean(absolute_error_y));
fprintf('Average translation error in X: %f\n', mean(absolute_error_x));
