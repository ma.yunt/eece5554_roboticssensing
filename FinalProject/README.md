# Final Project Group 5
## Presentation Slide
You can find the proposal slide in `Final_project_proposal.pdf`

You can find initial presentation slide in `Final_Presentation_First_Draft.pdf`

You can find the final presentation slide in `Final_Presentation_Final_Draft.pdf`

## Data processing pipline
You can find the detail example of ORB SLAM3 data processing pipline in `ORB_SLAM3 Instruction.pdf`. This file include detailed setup for ORB SLAM3, examples to run KITTI and EuRoC dataset with ORB SLAM3 Ros Mono node, and steps to convert results into analysis format. 

`get_kitti_cam_setting.py` script can be used to quickly parse KITTI dataset parameter into ymal format to create the setting files for ORB SLAM3.

**example**

```python3 get_kitti_cam_setting.py -d /home/tma/kitti/2011_09_26 -c 02 ```

`make_analysis_csv.py` combine the ground truth (GPS) data extracted from rosbag and the KeyFrameTrajectory file generated from the ORB SLAM3  into one standard analysis format for the analysis team.

**example**

```python3 make_analysis_csv.py -d kitti -t kitti_2011_09_26_0019_KFT.txt -g kitti_2011_09_26_0019_gt.csv```

## Analysis Script

See ground truth data(post-fix "gt"), key frame trajectory file (post-fix "kft"), and analysis data (post-fix "analysis") in `datasets` directory.

See analysis script in `analysis` directory. 