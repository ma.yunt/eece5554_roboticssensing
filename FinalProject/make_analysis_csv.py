import numpy as numpy
import pathlib
import argparse
import pandas as pd

def main(args):
    KFT_pth = pathlib.Path(args.KFT_pth)
    GT_pth = pathlib.Path(args.GT_pth)
    if not KFT_pth.exists():
        raise FileNotFoundError('Key Frame Trajectory file not found.')
    if not GT_pth.exists():
        raise FileNotFoundError('Ground truth file not found.')
    if args.dtype == 'kitti':
        gt_df = pd.read_csv(GT_pth)
        t_df = pd.read_csv(KFT_pth, delimiter = " ", header=None)

        out_df = pd.DataFrame()
        out_df['time_stamp'] = gt_df['field.header.stamp']
        out_df['latitude'] = gt_df['field.latitude']
        out_df['longitude'] = gt_df['field.longitude']
        out_df['altitude'] = gt_df['field.altitude']
        
        t_df[0] = t_df[0]*1e9
        t_df = t_df.astype({0: 'int64'})
        t_df = t_df.drop([4, 5, 6, 7], axis=1)
        t_df = t_df.rename(columns={0: 'time_stamp', 1: 'x', 2:'y', 3:'z'})
        out_df = out_df.merge(t_df, how = 'outer', on='time_stamp', sort=True)
        out_df.to_csv(args.out, index=False)
        
        

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dtype', dest='dtype',
                        help='dataset type',
                        choices=['kitti'],
                        required=True)
    parser.add_argument('-t', '--trajectory', dest='KFT_pth',
                        help='path for key frame trajectory file',
                        required=True)
    parser.add_argument('-g', '--groud_truth', dest='GT_pth',
                        help='path for ground truth file',
                        required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='output name',
                        default='analysis.csv')
    args = parser.parse_args()
    main(args)
