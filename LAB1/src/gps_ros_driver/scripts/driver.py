#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import rospy
import serial
import argparse
import utm
import std_msgs.msg

from gps_ros_driver.msg import Location

def nmeall_to_degree(nmeall):
    """convert NMEA lat lon data to degrees

    Args:
        nmeall (str): lat lon in ddmm.mmmm format

    Returns:
        (float): lat lon in degrees
        None: if condition not met
    """
    if nmeall == '':
        return None
    if nmeall[-1] == 'N' or nmeall[-1] == 'E':
        n = 1
    elif nmeall[-1] == 'S' or nmeall[-1] == 'W':
        n = -1
    else:
        return None
    
    nmeall = nmeall[:-1]
    dd = nmeall[:nmeall.find('.')-2]
    mm = nmeall[nmeall.find('.')-2:]
    if dd == '':
        dd  = 0.0
    if mm == '':
        mm = 0.0
    dd = float(dd) + (float(mm)/60)
    return dd*n
    

def parse_nema_gga(nmea_sentence):
    """parse GGA NMEA sentence

    Args:
        nmea_sentence (str): original nmea sentence

    Returns:
        (tuple): parsed NMEA location data and UTM data
    """
    # pre-processing
    nmea_sentence = nmea_sentence.split(',')
    # parser GGA data and return None if not GGA
    if nmea_sentence[0][3:] == 'GGA':
        raw = {'UTC': nmea_sentence[1], # UTC time in hhmmss.ss
               'lat': nmea_sentence[2] + nmea_sentence[3], # latitude in ddmm.mmmm + N/S
               'lon': nmea_sentence[4] + nmea_sentence[5], # longtitude in ddmm.mmmm + E/W
               'quality': nmea_sentence[6], # GPS quality indicator 
               'n_sat': nmea_sentence[7], # NUmer of satellite in use,
               'HDOP': nmea_sentence[8], # Horizontal dilution of precision
               'alt': nmea_sentence[9], # MSL (mean sea level) in meter 
               'geo_sep': nmea_sentence[11], # Geoidal Separation in meter
               'DGPS': nmea_sentence[14].split('*')[0], # Differential reference station ID
               'cks': nmea_sentence[14].split('*')[1]} # check sum
    else:
        return None
    # post processing
    lat = nmeall_to_degree(raw['lat'])
    lon = nmeall_to_degree(raw['lon'])
    if lat == None or lon == None:
        rospy.logwarn('Invalid Latitude or Longtitude.')
        return None
    alt = float(raw['alt'])
    easting, northing, zone_number, zone_letter = utm.from_latlon(lat, lon)
    return lat, lon, alt, easting, northing, zone_number, zone_letter

def main(args):
    """Main function - setup device and ROS publisher

    Args:
        args : Arguments object from argparse
    """
    rospy.init_node('GPS')
    h = std_msgs.msg.Header()
    pub = rospy.Publisher('gps_location', Location, queue_size=5)
    r = rospy.Rate(10)
    dev = serial.Serial(port=args.port, baudrate=args.baudrate, timeout=2)
    try:
        dev = serial.Serial(port=args.port, baudrate=args.baudrate, timeout=2)
        while not rospy.is_shutdown():
            nmea_setence = dev.readline().strip()
            data = parse_nema_gga(nmea_setence)
            if data is None:
                continue
            h.stamp = rospy.Time.now()
            pub.publish(h, *data)
            rospy.loginfo('Message:{}'.format(data))
            r.sleep()
        
    except rospy.ROSInternalException:
        dev.close()
    finally:
        dev.close()
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', dest='port', type=str,
                        required=True, help='serial port')
    parser.add_argument('-b', '--baudrate', dest='baudrate', type=int,
                        default=4800)
    args, unknown = parser.parse_known_args()
    main(args)
