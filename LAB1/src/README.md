**Driver Package**
ROS package gps_driver is under directory Gps-driver
note ROS package cannot contain "-", therefore the package is contain under gps-driver directory to comform with HW format requirement. 

Run the driver using `rosrun gps_ros_driver driver.py -p /dev/{dev name}`. 

**Report**
see Report.pdf 

**Analysis scripts**
see analysis_script.ipynb - a jupyter notebook in python3
map.png is for on-map plot in the analysis

**data**
still.bag and moving.bag are the original bag data collected using driver
still.csv and moving.csv are exported bag data using `rostopic echo -p`
