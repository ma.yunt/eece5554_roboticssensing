%--------------------------------------
% Yuntao Ma
% EECE5554 Robotic Sensing and Navigation
% Lab 3 - resize image
%--------------------------------------
originalFolder = 'original/';
resizedFolder = 'resize/';
namePattern = '*';
imformat = '.jpg';
imageFiles = dir([folderName, namePattern, imformat]);
scale = 0.25;
nfiles = length(imageFiles);    % Number of files found
for ind=1:nfiles
   currentFilename = imageFiles(ind).name;
   currentImage = imread([folderName, currentFilename]);
   resizedImage = imresize(currentImage, 0.25);
   imwrite(resizedImage, [resizedFolder, 'Image', int2str(ind), imformat])
end