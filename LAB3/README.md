**MATLAB file**

``harris.m`` and ``convolve2.m`` are original Harrison corner detecting script provided by the Lab.

``resize.m`` are used for resizing the image from the original directory to resize directory. One original image was kept in the original directory if you want to run the scirpt. 

``panormaic.m`` are main MATLAB scirpt to stiched image sets together referencing the MATLAB examples. The script use the undistorted images from undistorted directory. There are some parameter variable you can change in this scirpt:

`line11`: `nCorner` you can change this variable to change how many Harris coner you want to detect with `harris.m` function

`line13`: `show_intermedia` you can set this to true if you want to see intermediate image of the stiching process.

`line15`: `show_montage` you can set this to true if you want to see montage image of the each image set.

`line56`: `imSet` you can set this to (1) latino_center (2) brick (3) ruggles for the algorithm to run on different image sets. 

**Report**

See Report.pdf