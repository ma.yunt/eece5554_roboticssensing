%--------------------------------------
% Yuntao Ma
% EECE5554 Robotic Sensing and Navigation
% Lab 3 - panoramic script
%--------------------------------------
%% clear and hyperparameter 
clear
clc

% number of harris corner to detect 
nCorner = 2000;
% flag to see intermedia plot
show_intermedia = false;
% flag to see original image montage
show_montage = false;

%% load in image
imFolder = 'undistorted/';
namePattern = '*';
imFormat = '.jpg';
imageFiles = dir([imFolder, namePattern, imFormat]);
nfiles = length(imageFiles);    % Number of files found
I = {};
grayI = {};
for ind=1:nfiles
   currentFilename = imageFiles(ind).name;
   currentImage = imread([imFolder, currentFilename]);
   I{ind} = currentImage;
   grayImage = rgb2gray(currentImage);
   grayI{ind} = grayImage;
end
for ind = 1:6
    latino_center.rgb{ind} = I{1, ind};
    latino_center.gray{ind} = grayI{1, ind};
end
for ind = 1:7
    brick.rgb{ind} = I{1, ind+6};
    brick.gray{ind} = grayI{1, ind+6};
end
for ind = 1:6
    ruggles.rgb{ind} = I{1, ind+13};
    ruggles.gray{ind} = grayI{1, ind+13};
end
if show_montage
    figure
    montage(latino_center.rgb,'Size', [1 6])
    figure
    montage(brick.rgb,'Size', [1 7])
    figure
    montage(ruggles.rgb,'Size', [1 6])
end
%%----------------------------------------------------
% pick which image set you want to analysis here
% pick from (1) latino_center (2) brick (3) ruggles
%%---------------------------------------------------
imSet = latino_center;
%% part 5
% harris corner demo
[y,x,m]=harris(grayI{1,7},nCorner);
figure
image(I{1,7})
hold on
plot(x,y,'y+')
axis image
%% Register image paris
% the code reference from matlab example but modify to fit my own data
% structure.

% Initialize features for I(1)
% points = detectSURFFeatures(imSet.gray{1});
[y,x,m] = harris(imSet.gray{1},nCorner);
points = cornerPoints([x,y]);
[features, points] = extractFeatures(imSet.gray{1},points);

% Initialize all the transforms to the identity matrix. Note that the
% projective transform is used here because the building images are fairly
% close to the camera. Had the scene been captured from a further distance,
% an affine transform would suffice.
numImages = numel(imSet.gray);
tforms(numImages) = projective2d(eye(3));

% Initialize variable to hold image sizes.
imageSize = zeros(numImages,2);

% Iterate over remaining image pairs
for n = 2:numImages
    
    % Store points and features for I(n-1).
    pointsPrevious = points;
    featuresPrevious = features;
        
    grayImage = imSet.gray{n};
    
    % Save image size.
    imageSize(n,:) = size(grayImage);
    
    % Detect and extract SURF features for I(n).
    % points = detectSURFFeatures(grayImage); 
    [y,x,m] = harris(grayImage, nCorner);
    points = cornerPoints([x,y]);
    [features, points] = extractFeatures(grayImage, points);
  
    % Find correspondences between I(n) and I(n-1).
    indexPairs = matchFeatures(features, featuresPrevious, 'Unique', true);
       
    matchedPoints = points(indexPairs(:,1), :);
    matchedPointsPrev = pointsPrevious(indexPairs(:,2), :);        
    
    % show intermediate
    if show_intermedia
        figure 
        showMatchedFeatures(imSet.gray{n},imSet.gray{n-1},matchedPoints,matchedPointsPrev,'montage')
    end
    
    % Estimate the transformation between I(n) and I(n-1).
    tforms(n) = estimateGeometricTransform2D(matchedPoints, matchedPointsPrev,...
        'projective', 'Confidence', 99.9, 'MaxNumTrials', 2000);
    
    % Compute T(n) * T(n-1) * ... * T(1)
    tforms(n).T = tforms(n).T * tforms(n-1).T; 
end

% Compute the output limits for each transform.
for i = 1:numel(tforms)           
    [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(i,2)], [1 imageSize(i,1)]);    
end

avgXLim = mean(xlim, 2);
[~,idx] = sort(avgXLim);
centerIdx = floor((numel(tforms)+1)/2);
centerImageIdx = idx(centerIdx);

Tinv = invert(tforms(centerImageIdx));
for i = 1:numel(tforms)    
    tforms(i).T = tforms(i).T * Tinv.T;
end

%% Initialize the Panorama
for i = 1:numel(tforms)           
    [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(i,2)], [1 imageSize(i,1)]);
end

maxImageSize = max(imageSize);

% Find the minimum and maximum output limits. 
xMin = min([1; xlim(:)]);
xMax = max([maxImageSize(2); xlim(:)]);

yMin = min([1; ylim(:)]);
yMax = max([maxImageSize(1); ylim(:)]);

% Width and height of panorama.
width  = round(xMax - xMin);
height = round(yMax - yMin);

% Initialize the "empty" panorama.
panorama = zeros([height width 3], 'like', imSet.rgb{1});
if show_intermedia
    figure
    imshow(panorama)
end

%% Create the Panorama
blender = vision.AlphaBlender('Operation', 'Binary mask', ...
    'MaskSource', 'Input port');  

% Create a 2-D spatial reference object defining the size of the panorama.
xLimits = [xMin xMax];
yLimits = [yMin yMax];
panoramaView = imref2d([height width], xLimits, yLimits);

% Create the panorama.
for i = 1:numImages
    I = imSet.rgb{i};   
   
    % Transform I into the panorama.
    warpedImage = imwarp(I, tforms(i), 'OutputView', panoramaView);
    if show_intermedia
        figure
        imshow(warpedImage)
    end
                  
    % Generate a binary mask.    
    mask = imwarp(true(size(I,1),size(I,2)), tforms(i), 'OutputView', panoramaView);
    if show_intermedia
        figure
        imshow(mask)
    end
    % Overlay the warpedImage onto the panorama.
    panorama = step(blender, panorama, warpedImage, mask);
    if show_intermedia
        figure
        imshow(panorama)
    end
end

figure
imshow(panorama)
