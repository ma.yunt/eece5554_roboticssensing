#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import os
import rospy
import serial
import argparse
from math import pi
from tf_conversions import transformations
from std_msgs.msg import Header
from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import Quaternion

from imu_ros_driver.msg import Raw

def parse_imu_data(imu_sentence):
    # pre-processing
    VNYMR_sentence = imu_sentence.split(',')
    if VNYMR_sentence[0][1:] == 'VNYMR':
        raw = {'yaw': float(VNYMR_sentence[1]),
               'pitch': float(VNYMR_sentence[2]),
               'roll': float(VNYMR_sentence[3]),
               'magx': float(VNYMR_sentence[4]),
               'magy': float(VNYMR_sentence[5]),
               'magz': float(VNYMR_sentence[6]),
               'accelx': float(VNYMR_sentence[7]),
               'accely': float(VNYMR_sentence[8]),
               'accelz': float(VNYMR_sentence[9]),
               'gyrox': float(VNYMR_sentence[10]),
               'gyroy': float(VNYMR_sentence[11]),
               'gyroz': float(VNYMR_sentence[12].split('*')[0])}
    else:
        return None
    
    # post-processing and msg packaging
    # magnetic field
    mag_field_msg = Vector3(raw['magx'], raw['magy'], raw['magz'])
    # angular velocity
    ang_vel_msg = Vector3(raw['gyrox'], raw['gyroy'], raw['gyroz'])
    # linear acceleration
    lin_accel_msg = Vector3(raw['accelx'], raw['accely'], raw['accelz'])
    # orientation
    roll = raw['roll']/180*pi
    pitch = raw['pitch']/180*pi
    yaw = raw['yaw']/180*pi
    quat = transformations.quaternion_from_euler(roll, pitch, yaw)
    orient_msg = Quaternion(*quat)

    return orient_msg, ang_vel_msg, lin_accel_msg, mag_field_msg


def main(args):
    rospy.init_node('IMU')
    SENSOR_NAME = 'VN100'
    try:
        dev = serial.Serial(port=args.port, baudrate=args.baudrate, timeout=2)
    except serial.SerialException as e:
        rospy.logfatal('Cannot open serial port: {}'.format(e))
        return None
    imu_pub = rospy.Publisher(SENSOR_NAME + '/imu', Imu, queue_size=10)
    imu_header = Header()
    mag_pub = rospy.Publisher(SENSOR_NAME + '/mag', MagneticField, queue_size=10)
    mag_header = Header()
    
    if args.dump:
        raw_pub = rospy.Publisher(SENSOR_NAME + '/raw', Raw, queue_size=10)
        raw_header = Header()

    try:
        dev = serial.Serial(port=args.port, baudrate=args.baudrate, timeout=2)
        while not rospy.is_shutdown():
            imu_sentence = dev.readline().strip()
            if args.dump:
                raw_header.stamp = rospy.Time.now()
                raw_pub.publish(header = raw_header,
                                data = imu_sentence)
            data = parse_imu_data(imu_sentence)
            if data is None:
                continue
            imu_header.stamp = rospy.Time.now()
            imu_pub.publish(header = imu_header,
                            orientation = data[0], 
                            angular_velocity = data[1],
                            linear_acceleration = data[2])
            mag_header.stamp = rospy.Time.now()
            mag_pub.publish(header = mag_header,
                            magnetic_field = data[3])
            rospy.logdebug('Message:{}'.format(data))
    finally:
        dev.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', dest='port', type=str,
                        required=True, help='serial port')
    parser.add_argument('-b', '--baudrate', dest='baudrate', type=int,
                        default=115200)
    parser.add_argument('-d', '--dump', dest='dump',
                        action='store_true', 
                        help='dump original data in VN100/raw topic')
    args, unknown = parser.parse_known_args()
    main(args)
