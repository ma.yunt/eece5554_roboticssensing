%--------------------------------------
% Yuntao Ma
% EECE5554 Robotic Sensing and Navigation
% Lab 2 - dead-reckoning
%--------------------------------------
%% Reading data from bagfile
% ROS Toolbox needed
bag = rosbag('driving.bag');
bSel = select(bag, 'Topic','/VN100/imu');
imu_msg = readMessages(bSel,'DataFormat','struct');
bSel = select(bag, 'Topic','/gps_location');
gps_msg = readMessages(bSel,'DataFormat','struct');
bSel = select(bag, 'Topic','/VN100/mag');
mag_msg = readMessages(bSel,'DataFormat','struct');
%% Data extraction
for i = 1:bSel.NumMessages
    accel_x(i,1)=imu_msg{i,1}.LinearAcceleration.X;
    accel_y(i,1)=imu_msg{i,1}.LinearAcceleration.Y;
    accel_z(i,1)=imu_msg{i,1}.LinearAcceleration.Z;
    angular_x(i,1)=imu_msg{i,1}.AngularVelocity.X;
    angular_y(i,1)=imu_msg{i,1}.AngularVelocity.Y;
    angular_z(i,1)=imu_msg{i,1}.AngularVelocity.Z;
    mag_x(i,1)=mag_msg{i, 1}.MagneticField.X;
    mag_y(i,1)=mag_msg{i, 1}.MagneticField.Y;
    mag_z(i,1)=mag_msg{i, 1}.MagneticField.Z;
    q = [imu_msg{i, 1}.Orientation.W imu_msg{i, 1}.Orientation.X ...
        imu_msg{i, 1}.Orientation.Y imu_msg{i, 1}.Orientation.Z];
    quat(i,1)=quaternion([q(1), q(2), q(3), q(4)]);
end
yaw = euler(quat,'zyx','point');
yaw = yaw(:,1);

for i = 1:402
    easting(i,1) = gps_msg{i, 1}.UtmEasting;
    northing(i,1) = gps_msg{i, 1}.UtmNorthing;
end
%% Examine mag time series plot to select 
signal = mag_x;
t = size(signal);
t = linspace(1, t(1), t(1))./40+1;
figure(1)
plot(t, signal)
xlabel('time(s)')
ylabel('mag x')
title('mag x')
%% Mag_cal data and driving data seperation
t_cal = 48.4*40;
t_drv = 118*40;
t_end = bSel.NumMessages;

mag_cal_x =  mag_x(t_cal:t_drv,1);
mag_cal_y =  mag_y(t_cal:t_drv,1);
mag_cal_z =  mag_z(t_cal:t_drv,1);
mag_drv_x =  mag_x(t_drv:t_end,1);
mag_drv_y =  mag_y(t_drv:t_end,1);
mag_drv_z =  mag_z(t_drv:t_end,1);
accel_drv_x =  accel_x(t_drv:t_end,1);
accel_drv_y =  accel_y(t_drv:t_end,1);
angular_drv_x =  angular_x(t_drv:t_end,1);
angular_drv_y =  angular_y(t_drv:t_end,1);
angular_drv_z =  angular_z(t_drv:t_end,1);
yaw_drv = yaw(t_drv:t_end,1);
easting_drv = easting(118:402,1);
northing_drv = northing(118:402,1);
%% Hard-iron, soft-iron mag calibration
mag_cal = [mag_cal_x, mag_cal_y, mag_cal_z];
[A,b,expmfs] = magcal(mag_cal,'auto');
figure(2)
plot(mag_cal_x, mag_cal_y)
mag_corrected = (mag_cal-b)*A;
hold on 
plot(mag_corrected(:,1), mag_corrected(:,2))
title('Magnetometer Calibration')
legend('Before calibration', 'After calibration')
xlabel('H_X (Gauss)')
ylabel('H_Y (Gauss)')
axis equal 
grid on 
%% extract yaw
% correct driving mag data
x = size(accel_drv_x);
x = linspace(1, x(1), x(1))./40+1;
mag_drv = [mag_drv_x, mag_drv_y, mag_drv_z];
mag_drv = (mag_drv-b)*A;
yaw_from_gyro = unwrap(cumtrapz(x,angular_drv_z));
yaw_from_mag = unwrap(atan2(-mag_drv(:,2),mag_drv(:,1)));

% plot yaw from gyro vs yaw from mag
figure(3)
signal = yaw_from_gyro;
t = size(signal);
t = linspace(1, t(1), t(1))./40+1;
plot(t, signal)
hold on
signal = yaw_from_mag;
t = size(signal);
t = linspace(1, t(1), t(1))./40+1;
plot(t, signal)
title('Heading angle estimation from Magnetometer and Gyroscope')
xlabel('Time(s)')
ylabel('Yaw (rad)')
legend('Yaw from gyroscope', 'Yaw from magnetometer')
grid on
% signal = unwrap(yaw_drv);
% t = size(signal);
% t = linspace(1, t(1), t(1))./40+1;
% plot(t, signal)
%% complementary filter 
wpass = 7e-3; % Normalized passband frequency
yaw_from_gyro_f = highpass(yaw_from_gyro,wpass);
yaw_from_mag_f = lowpass(yaw_from_mag,wpass);
combine_yaw = yaw_from_gyro_f+yaw_from_mag_f + yaw_drv(1,1);

% filter signal 
figure(4)
signal = yaw_from_gyro_f;
t = size(signal);
t = linspace(1, t(1), t(1))./40+1;
plot(t, signal)
hold on
signal = yaw_from_mag_f;
t = size(signal);
t = linspace(1, t(1), t(1))./40+1;
plot(t, signal)
hold off

% combine signal compare to the yaw 
figure(5)
% signal = yaw_drv;
signal = unwrap(yaw_drv);
t = size(signal);
t = linspace(1, t(1), t(1))./40+1;
plot(t, signal)
hold on
% signal = wrapToPi(combine_yaw);
signal = combine_yaw;
t = size(signal);
t = linspace(1, t(1), t(1))./40+1;
plot(t, signal)
title('IMU Yaw vs. Complementary filter result')
xlabel('Time(s)')
ylabel('Yaw (rad)')
legend('IMU Yaw', 'Complementary filtered Yaw')
grid on
hold off

%% Estimate forward velocity initially
x = size(accel_drv_x);
x = linspace(1, x(1), x(1))./40+1;
vel_est = cumtrapz(x,accel_drv_x);
vel_gps = zeros(284,1);
for i = 1:283
    vel_gps (i) = sqrt((easting_drv(i+1)-easting_drv(i))^2 + ...
        (northing_drv(i+1)-northing_drv(i))^2);
end

figure(7)
signal = vel_gps;
t = size(signal);
t = linspace(1, t(1), t(1));
plot(t, signal)
hold on
signal = vel_est;
t = size(signal);
t = linspace(1, t(1), t(1))./40+1;
plot(t, signal)
title('Initial forward velocity estimation')
xlabel('Time(s)')
ylabel('Velocity (m/s)')
legend('GPS', 'IMU')
grid on
hold off
%% Estimate forward velocity adjustment
% add bias and initialize velocity 
accel_drv_x_adjust = accel_drv_x+0.095;
k=0;
vel_est_adjust = [];
% iterate through gps velocity by segment when gps velocity is 0, adjust
% the acceleration to 0 and intergrate the previouse segment, and repeat. 
for i = 0:283
    if vel_gps(i+1) == 0
        accel_drv_x_adjust(i*40+1:i*40+41) = 0;
        temp = cumtrapz(x(k*40+1:i*40+1),...
            accel_drv_x_adjust(k*40+1:i*40+1))+vel_gps(k+1);
        vel_est_adjust = [vel_est_adjust; temp];
        k = i;
    end
end
% adjust for n 
vel_est_adjust= [vel_est_adjust; 0];

figure(8)
subplot(2,1,1)
signal = accel_drv_x_adjust;
t = size(signal);
t = linspace(1, t(1), t(1))./40+1;
plot(t, signal)
title('Adjusted X-axis Acceleration data')
xlabel('acceleration (m/s^2)')
ylabel('time(s)')
grid on

subplot(2,1,2)
signal = vel_gps;
t = size(signal);
t = linspace(1, t(1), t(1));
plot(t, signal)
hold on
signal = vel_est_adjust;
t = size(signal);
t = linspace(1, t(1), t(1))./40+1;
plot(t, signal)
title('Adjusted forward velocity estimation')
xlabel('Time(s)')
ylabel('Velocity (m/s)')
legend('GPS', 'IMU')
grid on
hold off
%% Dead Reckoning Part 1
X_2 = accel_drv_x;
X_1 = cumtrapz(x, accel_drv_x);
wX_1 = angular_drv_z .* X_1;

figure(9)
signal = accel_drv_y;
t = size(signal);
t = linspace(1, t(1), t(1))./40+1;
plot(t, signal)
hold on
signal = -wX_1;
t = size(signal);
t = linspace(1, t(1), t(1))./40+1;
plot(t, signal)
title("-\omegaX' vs. Y''")
xlabel('Time(s)')
ylabel('Acceleration (m/s^2)')
legend('Y"', "-\omegaX'")
hold off
%% Dead Reckoning Part 2
% combine yaw
vn = cos(combine_yaw+0.7).*vel_est_adjust;
ve = sin(combine_yaw+0.7).*vel_est_adjust;
xn = cumtrapz(x, vn)+northing_drv(1,1);
xe = cumtrapz(x, ve)+easting_drv(1,1);
figure(10)
plot(easting_drv, northing_drv)
hold on
plot(xe, xn)
title("Position estimation using Gyro, Mag heading")
xlabel('UTM Easting (m)')
ylabel('UTM Northing (m)')
legend('GPS', 'IMU')
axis equal

% imu yaw
vn = cos(yaw_drv+0.7).*vel_est_adjust;
ve = sin(yaw_drv+0.7).*vel_est_adjust;
xn = cumtrapz(x, vn)+northing_drv(1,1);
xe = cumtrapz(x, ve)+easting_drv(1,1);
figure(11)
plot(easting_drv, northing_drv)
hold on
plot(xe, xn)
title("Position estimation using IMU heading")
xlabel('UTM Easting (m)')
ylabel('UTM Northing (m)')
legend('GPS', 'IMU')
axis equal

%% Estimate xc
Y_2 = accel_drv_y;
Y_1 = cumtrapz(x, accel_drv_y);
w = angular_drv_z;
w_1 = [diff(w); 0];

xc = (-w(2,1).*Y_1(2,1))./(w(2,1).^2+w_1(2,1));

