%--------------------------------------
% Yuntao Ma
% EECE5554 Robotic Sensing and Navigation
% Lab 2 - find allen deviation of IMU
%--------------------------------------
%% Reading data from bagfile
% ROS Toolbox needed
bag = rosbag('five-hour.bag');
bSel = select(bag, 'Topic','/VN100/imu');
imu_msg = readMessages(bSel,'DataFormat','struct');
%% data extraction
accel_x = zeros(bSel.NumMessages,1);
for i = 1:bSel.NumMessages
    accel_x(i,1)=imu_msg{i,1}.LinearAcceleration.X;
    accel_y(i,1)=imu_msg{i,1}.LinearAcceleration.Y;
    accel_z(i,1)=imu_msg{i,1}.LinearAcceleration.Z;
    angular_x(i,1)=imu_msg{i,1}.AngularVelocity.X;
    angular_y(i,1)=imu_msg{i,1}.AngularVelocity.Y;
    angular_z(i,1)=imu_msg{i,1}.AngularVelocity.Z;
end
%% Plot signal time series
t = linspace(1, bSel.NumMessages,bSel.NumMessages)./40;
signal = angular_x;
figure
plot(t, signal)
xlabel('time(s)')
ylabel('Angular Velocity(\circ)')
title('Gyroscope X-axis Time Series Data')

%% Allan Variace Calculation
% Manual calculation
% assigne signal here
omega = accel_x; 
Fs = 40; % sample rate 
t0 = 1/Fs; % sample time
maxNumM = 100;

theta = cumsum(omega, 1)*t0;
L = size(theta, 1);
maxM = 2.^floor(log2(L/2));
m = logspace(log10(1), log10(maxM), maxNumM).';
m = ceil(m); % m must be an integer.
m = unique(m); % Remove duplicates.

tau = m*t0;

avar = zeros(numel(m), 1);
for i = 1:numel(m)
    mi = m(i);
    avar(i,:) = sum( ...
        (theta(1+2*mi:L) - 2*theta(1+mi:L-mi) + theta(1:L-2*mi)).^2, 1);
end
avar = avar ./ (2*tau.^2 .* (L - 2*m));
adev = sqrt(avar);

figure(1)
loglog(tau, adev)
title('Allan Deviation')
xlabel('\tau');
ylabel('\sigma(\tau)')
grid on
axis equal
hold on 

% Use allanvar function from Sensor Fusion and Tracking Toolbox
[avarFromFunc, tauFromFunc] = allanvar(omega, m, Fs);
adevFromFunc = sqrt(avarFromFunc);

loglog(tau, adev, tauFromFunc, adevFromFunc);
title('Allan Deviations')
xlabel('\tau')
ylabel('\sigma(\tau)')
legend('Manual Calculation', 'allanvar Function')
grid on
axis equal

%% Angle Random Walk N
% Find the index where the slope of the log-scaled Allan deviation is equal
% to the slope specified.
slope = -0.5;
logtau = log10(tau);
logadev = log10(adev);
dlogadev = diff(logadev) ./ diff(logtau);
[~, i] = min(abs(dlogadev - slope));

% Find the y-intercept of the line.
b = logadev(i) - slope*logtau(i);

% Determine the angle random walk coefficient from the line.
logN = slope*log(1) + b;
N = 10^logN

% Plot the results.
tauN = 1;
lineN = N ./ sqrt(tau);
figure
loglog(tau, adev, tau, lineN, '--', tauN, N, 'o')
title('Allan Deviation with Angle Random Walk')
xlabel('\tau')
ylabel('\sigma(\tau)')
legend('\sigma', '\sigma_N')
text(tauN, N, 'N')
grid on
axis equal

%% Rate Random Walk K
% Find the index where the slope of the log-scaled Allan deviation is equal
% to the slope specified.
slope = 0.5;
logtau = log10(tau);
logadev = log10(adev);
dlogadev = diff(logadev) ./ diff(logtau);
[~, i] = min(abs(dlogadev - slope));

% Find the y-intercept of the line.
b = logadev(i) - slope*logtau(i);

% Determine the rate random walk coefficient from the line.
logK = slope*log10(3) + b;
K = 10^logK

% Plot the results.
tauK = 3;
lineK = K .* sqrt(tau/3);
figure
loglog(tau, adev, tau, lineK, '--', tauK, K, 'o')
title('Allan Deviation with Rate Random Walk')
xlabel('\tau')
ylabel('\sigma(\tau)')
legend('\sigma', '\sigma_K')
text(tauK, K, 'K')
grid on
axis equal

%% Bias Instability
% Find the index where the slope of the log-scaled Allan deviation is equal
% to the slope specified.
slope = 0;
logtau = log10(tau);
logadev = log10(adev);
dlogadev = diff(logadev) ./ diff(logtau);
[~, i] = min(abs(dlogadev - slope));

% Find the y-intercept of the line.
b = logadev(i) - slope*logtau(i);

% Determine the bias instability coefficient from the line.
scfB = sqrt(2*log(2)/pi);
logB = b - log10(scfB);
B = 10^logB

% Plot the results.
tauB = tau(i);
lineB = B * scfB * ones(size(tau));
figure
loglog(tau, adev, tau, lineB, '--', tauB, scfB*B, 'o')
title('Allan Deviation with Bias Instability')
xlabel('\tau')
ylabel('\sigma(\tau)')
legend('\sigma', '\sigma_B')
text(tauB, scfB*B, '0.664B')
grid on
axis equal

%% All three noise parameter
tauParams = [tauN, tauK, tauB];
params = [N, K, scfB*B];
figure
loglog(tau, adev, tau, [lineN, lineK, lineB], '--', ...
    tauParams, params, 'o')
title('Gyroscope X-Axis Allan Deviation with Noise Parameters')
xlabel('Sample time \tau (s)')
ylabel('Allan Deviation \sigma(\tau) (\circ/\surds)')
legend('\sigma', '\sigma_N', '\sigma_K', '\sigma_B')
text(tauParams, params, {'N', 'K', '0.664B'})
grid on
axis equal

%% Gyroscope simulation
% Simulating the gyroscope measurements takes some time. To avoid this, the
% measurements were generated and saved to a MAT-file. By default, this
% example uses the MAT-file. To generate the measurements instead, change
% this logical variable to true.
generateSimulatedData = true;

if generateSimulatedData
    % Set the gyroscope parameters to the noise parameters determined
    % above.
    gyro = gyroparams('NoiseDensity', N, 'RandomWalk', K, ...
        'BiasInstability', B);
    omegaSim = helperAllanVarianceExample(L, Fs, gyro);
else
    load('SimulatedSingleAxisGyroscope', 'omegaSim')
end

[avarSim, tauSim] = allanvar(omegaSim, 'octave', Fs);
adevSim = sqrt(avarSim);
adevSim = mean(adevSim, 2); % Use the mean of the simulations.

figure
loglog(tau, adev, tauSim, adevSim, '--')
title('Allan Deviation of HW and Simulation')
xlabel('\tau');
ylabel('\sigma(\tau)')
legend('HW', 'SIM')
grid on
axis equal

function omegaSim = helperAllanVarianceExample(L, Fs, gyro, iters)
%HELPERALLANVARIANCEEXAMPLE Helper function used by AllanVarianceExample
%   OMEGASIM = HELPERALLANVARIANCEEXAMPLE(L, FS, GYRO) returns an L-by-3
%   matrix OMEGASIM containing 3 different simulations of a stationary
%   gyroscope with parameters GYRO and sampling rate FS.
%     
%   OMEGASIM = HELPERALLANVARIANCEEXAMPLE(L, FS, GYRO, ITERS) returns an 
%   L-by-ITERS matrix OMEGASIM.
%
%   
%   This function is for use with AllanVarianceExample only. It may be
%   removed in the future.

%   Copyright 2018-2019 The MathWorks, Inc.

if nargin < 4
    iters = 3;
end

% Create stationary input.
acc = zeros(L, 3);
angvel = zeros(L, 3);

omegaSim = zeros(L,iters);
for i = 1:iters
    imu = imuSensor('SampleRate', Fs, 'Gyroscope', gyro, ...
    'RandomStream', 'mt19937ar with seed', 'Seed', i);
    fprintf('Running simulation %d of %d ...\n', i, iters);
    [~, gyroDataSim] = imu(acc, angvel);
    omegaSim(:,i) = gyroDataSim(:,1);
end
end