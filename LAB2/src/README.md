**Driver Package**

Run the imu_ros_driver use `rosrun imu_ros_driver driver.py [ARG]`, see `[ARG]` usage below

```
usage: driver.py [-h] -p PORT [-b BAUDRATE] [-d]

optional arguments:
  -h, --help            show this help message and exit
  -p PORT, --port PORT  serial port
  -b BAUDRATE, --baudrate BAUDRATE
  -d, --dump            dump original data in VN100/raw topic
```

**Report**

*Part1*: see analysis/Report_IMU_Part1.pdf

*Part2*: see analysis/Report_IMU_Part2.pdf

**Analysis scripts**

*Part1*

see analysis_script_part1.ipynb - a jupyter notebook in python3

*Part2*

see dead_reckoning.m for part 2.b

see allen_deviation.m for part 2.c

**data**

*Part1*

imu_part1.bag are the original bag data collected using driver for part1

imu_part1.csv and mag_part1.csv are exported bag data using `rostopic echo -p`


*Part2*

bag data too big for upload to GitLab, check team channel file. 
